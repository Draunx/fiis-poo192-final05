package eu.uni.fiis;

public class Usuario {
    private String userId;
    private String password;
    private String loginStatus;
    private Integer registerDate;


    public Usuario(String userId, String password, String loginStatus, Integer registerDate) {
        this.userId = userId;
        this.password = password;
        this.loginStatus = loginStatus;
        this.registerDate = registerDate;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void verifyLogin(){
        System.out.println("Verificado");
    }
}
