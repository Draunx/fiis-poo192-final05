package eu.uni.fiis;

public class Pedido {
    private int orderld;
    private String dateCreated;
    private String dateShipped;
    private String customerName;
    private String customerId;
    private String status;
    private String shippingId;

    public Pedido(int orderld, String dateCreated, String dateShipped, String customerName, String customerId, String status, String shippingId) {
        this.orderld = orderld;
        this.dateCreated = dateCreated;
        this.dateShipped = dateShipped;
        this.customerName = customerName;
        this.customerId = customerId;
        this.status = status;
        this.shippingId = shippingId;
    }

    public void placeOrder(){

    }
}
