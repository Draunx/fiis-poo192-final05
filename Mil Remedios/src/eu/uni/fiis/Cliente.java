package eu.uni.fiis;

public class Cliente extends Usuario{

    private String customerName;
    private String adress;
    private String email;
    private String creditCardInfo;
    private String sippingInfo;
    private float accountBalance;

    public Cliente(String userId, String password, String loginStatus, Integer registerDate, String customerName, String adress, String email, String creditCardInfo, String sippingInfo, float accountBalance) {
        super(userId, password, loginStatus, registerDate);
        this.customerName = customerName;
        this.adress = adress;
        this.email = email;
        this.creditCardInfo = creditCardInfo;
        this.sippingInfo = sippingInfo;
        this.accountBalance = accountBalance;
    }

    public void register(){
        System.out.println("Registrado");
    }
    public void login() {
        System.out.println("Logueado");
    }
    public void updateProfile(){
        System.out.println("Update Complete");
    }
}
