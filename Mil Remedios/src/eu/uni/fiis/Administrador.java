package eu.uni.fiis;

public class Administrador extends Usuario {
    private String adminName;
    private String email;

    public Administrador(String userId, String password, String loginStatus, Integer registerDate, String adminName, String email) {
        super(userId, password, loginStatus, registerDate);
        this.adminName = adminName;
        this.email = email;
    }

    public void updateCatalog(){
        System.out.println("Update Complete");
    }
}
